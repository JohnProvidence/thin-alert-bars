<!DOCTYPE>
<html>

<head>
	<title>WPRI PinPoint Weather Thinbar </title>
	<link rel="stylesheet" type="text/css" href="css/pp-thinbar.css" media="screen" />
<head>

	<body class="TBarAdmin">

				<nav class="adminNav">
<span class="mainReturn"><a href="http://wpri.lininteractive.com/modules/admin/modules">Return to Main</a></span>
		</nav>

		<div id="TBarSelect">
			<h2>Thinbar Builder</h2>
			<p>Select the Thinbar you would like to add or modify content for</p>

			
				
					<ul class="mainAdmin">
  						<li><a href="pinpointweather/pinpoint-admin.php"><h3>Pinpoint Weather Alerts</h3><img src="images/PinpointWeather.png" /></a></li>
  						<li><a href="Breaking-News/breaking-admin.php"><h3>Breaking News</h3><img src="images/BreakingNews.png" /></a></li>
  						<li><a href="live-streaming/livestreaming-admin.php"><h3>Live Streaming</h3><img src="images/LiveStreaming.png" /></a></li>
  						<li><a href="Photo-Gallery/photo-admin.php"><h3>Photo Gallery</h3><img src="images/PhotoGallery.png" /></a></li>
  						<li><a href="Nesis-Notes/nesi-admin.php"><h3>Nesi's Notes</h3><img src="images/NesisNotes.png" /></a></li>
  						<li><a href="call12/call12-admin.php"><h3>Call 12 For Action</h3><img src="images/Call12ForAction.png" /></a></li>
  						<li><a href="Report-It/reportit-admin.php"><h3>Report It!</h3><img src="images/ReportIt.png" /></a></li>
  						<li><a href="Target-12/target12-admin.php"><h3>Target 12</h3><img src="images/Target12.png" /></a></li>
  						<li><a href="Special-Event/specialevent-admin.php"><h3>Special Event</h3><img src="images/SpecialEvent.png" /></a></li>
  						<li><a href="Continuing-Coverage/continuing-admin.php"><h3>Continuing Coverage</h3><img src="images/ContinuingCoverage.png" /></a></li>
  						<li><a href="Variable-Content/variable-admin.php"><h3>Variable Thinbar</h3><img src="images/Blue-Gradient.png" /></a></li>
					</ul>
		</div>

	</body>
<html>
