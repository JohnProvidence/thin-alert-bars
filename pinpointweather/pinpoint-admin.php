
<!DOCTYPE>
<html>

<head>
	<title>WPRI Thinbar Admin</title>
	<link rel="stylesheet" type="text/css" href="../css/pp-thinbar.css" media="screen" />
<head>

	<body>
				<nav class="adminNav">
<span class="mainReturn"><a href="../index.php">Return to Main</a></span>
		</nav>
		
<div id="TextEntry">
		<h2>Modify Pinpoint Thinbar</h2>

		<p>Add headline text and a link to the thinbar.</p>
		<span class="notes">Headline text is limited to 50 characters.</span>
		<span class="notes">Please enter full url for links. Ex: <span class="url-link"> http://www.wpri.com/</span> 

		<form action="add-content.php?id=1" method="POST" enctype="multipart/form-data">
		
		<fieldset>
			<ul>
			<li><span class="notes">Select the type of weather alert:</span></li>
			<li><input type="radio" name="weather_image" value="background:url(http://wpri.lininteractive.com/modules/mod.thinbars/images/PinpointWeather.png) no-repeat;width:177px;height:25px;"/>Pinpoint Weather</li>
			<li><input type="radio" name="weather_image"  value="background:url(http://wpri.lininteractive.com/modules/mod.thinbars/images/PinpointWeatherAlert.png) no-repeat;width:232px;height:25px;"/>Pinpoint Weather Alert</li>
  			<li><input type="radio" name="weather_image"  value="background:url(http://wpri.lininteractive.com/modules/mod.thinbars/images/SevereWeatherAlert.png) no-repeat;;width:229px;height:25px;"/>Severe Weather Alert</li>
<?php
include '../connect.php';

// select record from mysql
$sql="SELECT * FROM $tbl_name WHERE pp_id='1'";
$result=mysql_query($sql);
?>

<?php
while($rows=mysql_fetch_array($result)){
?>
			<li><label for="pp_headline">Pinpoint Thinbar Headline</label> <textarea name="pp_headline"><?php echo $rows['pp_headline']; ?></textarea></li>
			<li><label for="pp_url">Article Link:</label> <textarea name="pp_url"><?php echo $rows['pp_url']; ?></textarea></li>
			</fieldset>

		 <fieldset class="center">
        <input type="submit" value="Submit" class="button" name="submit" />
      </fieldset>

</form>

  </div>
<?php
// close while loop
}
?>


<?php
// close connection;
mysql_close();
?>

<div id="Container">
  <h2>Preview:</h2>
  <iframe src="pp-output.php" name="ThinBarframe" height="auto" width="100%" frameborder="0" scrolling="yes" >

</div>


</body>
</html>