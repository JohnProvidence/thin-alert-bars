
<!DOCTYPE>
<html>


<head>
	<title>WPRI Thinbar Admin</title>
	<link rel="stylesheet" type="text/css" href="../css/pp-thinbar.css" media="screen" />
<head>

	<body>
		<nav class="adminNav">
<span class="mainReturn"><a href="../index.php">Return to Main</a></span>
		</nav>

<div id="TextEntry">
		<h2>Modify Variable Content Thinbar</h2>

		<p>Add headline text and a link to the thinbar.</p>
		<span class="notes">Headline text is limited to 50 characters.</span>
		<span class="notes">Please enter full url for links. Ex: <span class="url-link"> http://www.wpri.com/</span> 

		<form action="add-content.php?id=16" method="POST" enctype="multipart/form-data">

<?php
include '../connect.php';

// select record from mysql
$sql="SELECT * FROM $tbl_name WHERE pp_id='16'";
$result=mysql_query($sql);
?>

<?php
while($rows=mysql_fetch_array($result)){
?>

		
		<fieldset>
			<ul>
				<li><h3>Choose the Thinbar background color</h3></li>
				<li><input type="radio" name="bg_color" value="PPW"/>Variable Red</li>
			<li><input type="radio" name="bg_color"  value="LS"/>Variable Blue</li>
  			<li><input type="radio" name="bg_color"  value="CC"/>Variable Silver</li>
  			<li><h3>Choose the Thinbar header background</h3></li>
  			<li><input type="radio" name="weather_image" value="http://wpri.lininteractive.com/modules/mod.thinbars/images/Red-Gradient.png"/><img src="http://wpri.lininteractive.com/modules/mod.thinbars/images/Red-Gradient.png" /></li>
			<li><input type="radio" name="weather_image"  value="http://wpri.lininteractive.com/modules/mod.thinbars/images/Blue-Gradient.png"/><img src="http://wpri.lininteractive.com/modules/mod.thinbars/images/Blue-Gradient.png" /></li>
  			<li><input type="radio" name="weather_image"  value="http://wpri.lininteractive.com/modules/mod.thinbars/images/Silver-Gradient.png"/><img src="http://wpri.lininteractive.com/modules/mod.thinbars/images/Silver-Gradient.png" /></li>
			<li><lable for="variable_header">Variable Header</lable> <textarea name="variable_header"><?php echo $rows['variable_header']; ?></textarea></li>
			<li><label for="pp_headline">Pinpoint Thinbar Headline</label> <textarea name="pp_headline"><?php echo $rows['pp_headline']; ?></textarea></li>
			<li><label for="pp_url">Article Link:</label> <textarea name="pp_url"><?php echo $rows['pp_url']; ?></textarea></li>
			</fieldset>

		 <fieldset class="center">
        <input type="submit" value="Submit" class="button" name="submit" />
      </fieldset>

</form>

  </div>
<?php
// close while loop
}
?>


<?php
// close connection;
mysql_close();
?>

<div id="Container">
  <h2>Preview:</h2>
  <iframe src="variable-output.php" name="ThinBarframe" height="auto" width="100%" frameborder="0" scrolling="yes" >

</div>


</body>
</html>